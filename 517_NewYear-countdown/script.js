const year = document.getElementById('year');
const days = document.getElementById('days');
const hours = document.getElementById('hours');
const minutes = document.getElementById('minutes');
const seconds = document.getElementById('seconds');

const countdown = document.getElementById('countdown');
const loading = document.getElementById('loading');

const currentYear = new Date().getFullYear();

const newYearTime = new Date(`January 01 ${currentYear + 1} 00:00:00`);

// Set background year
year.innerText = currentYear + 1;

function updateCountdown() {
  const currentTime = new Date();
  console.log(newYearTime, currentYear, currentTime);
  const diffUntilNY = newYearTime - currentTime;

  // Total time remaining
  // ms -> s
  const secondsUntilNY = Math.floor(diffUntilNY / 1000);
  const minutesUntilNY = Math.floor(diffUntilNY / 1000 / 60);
  const hoursUntilNY = Math.floor(diffUntilNY / 1000 / 60 / 60);
  const daysUntilNY = Math.floor(diffUntilNY / 1000 / 60 / 60 / 24);

  // console.log(daysUntilNY, hoursUntilNY, minutesUntilNY, secondsUntilNY);

  // only remining, no full Day,Hour,Second. 
  // Fraction of (Day,Hour,Minute,Second) remaining
  // count time passed
  const d = Math.floor(diffUntilNY / 1000 / 60 / 60 / 24);
  const h = Math.floor(diffUntilNY / 1000 / 60 / 60) % 24;
  const m = Math.floor(diffUntilNY / 1000 / 60) % 60;
  const s = Math.floor(diffUntilNY / 1000) % 60;

  // console.log(d, h, m, s);
  days.innerHTML = d;
  hours.innerHTML = decimals(h);
  minutes.innerHTML = decimals(m);
  seconds.innerHTML = decimals(s);
}

function decimals(time) {
  return time < 10 ? '0' + time : time
}

// Show spinner before countdown
setTimeout(() => {
  loading.remove();
  countdown.style.display = 'flex';
}, 1000);

// updateCountdown();
setInterval(updateCountdown, 1000);