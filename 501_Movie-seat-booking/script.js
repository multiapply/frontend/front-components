const container = document.querySelector('.container');
const emptySeats = document.querySelectorAll('.row .seat:not(.occupied)');
const seats = document.querySelectorAll('.row .seat');

const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');


populateUI();
// Pick a movie
let ticketPrice = +movieSelect.value;
updateCheckoutSelectedCount();


// Save selected movie index and price
function setMovieData(movieIndex, moviePrice) {
  localStorage.setItem('selectedMovieIndex', movieIndex);
  localStorage.setItem('selectedMoviePrice', moviePrice);
}

// update total and count
function updateCheckoutSelectedCount(){
  const selectedSeats = document.querySelectorAll('.row .seat.selected');
  
  // save each selected seats
  const seatsIndex = [...selectedSeats].map(seat =>
    [...emptySeats].indexOf(seat)
  );
  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex))


  const selectedSeatsCount = selectedSeats.length;

  count.innerText = selectedSeatsCount;
  total.innerText = selectedSeatsCount * ticketPrice;
}



// [EVENTS]
// Get data from localstorage and populate UI
function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));
  if(selectedSeats !== null && selectedSeats.length > 0){
    emptySeats.forEach((seat, index) => {
      if(selectedSeats.indexOf(index) > -1) {
        seat.classList.add('selected')
      }
    })
  }
  selectedMovieIndex = localStorage.getItem('selectedMovieIndex')
  // selectedMovieIndex = localStorage.getItem('selectedMoviePrice')

  if(selectedMovieIndex !== null){
    movieSelect.selectedIndex = selectedMovieIndex;
  }
}

// movie select event - Dropdown
movieSelect.addEventListener('change', e=> {
  ticketPrice = +e.target.value;
  setMovieData(e.target.selectedIndex, e.target.value);
  updateCheckoutSelectedCount();
})

// Seat click event
container.addEventListener('click', e=> {
  if(e.target.classList.contains('seat') 
    && !e.target.classList.contains('occupied')
  ){
    e.target.classList.toggle('selected');
    updateCheckoutSelectedCount();
  }
})

