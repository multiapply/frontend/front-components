const progress = document.getElementById('progress');
const prev = document.getElementById('prev');
const next = document.getElementById('next');
const circles = document.querySelectorAll('.circle');

let currentActive = 1;

next.addEventListener('click', () => {
  currentActive++;
  if (currentActive > circles.length) {
    currentActive = circles.length
  }
  updateProgressBar();
})

prev.addEventListener('click', () => {
  currentActive--;
  if (currentActive < 1) {
    currentActive = 1
  }
  updateProgressBar();
})

function updateProgressBar() {
  // Update circles
  circles.forEach((circle, idx) => {
    if(idx < currentActive){
      circle.classList.add('active')
    } else {
      circle.classList.remove('active')
    }
  })
  // Update progressLine
  const actives = document.querySelectorAll('.active')
  const progressPercentage = ((actives.length - 1)  / (circles.length - 1)) * 100
  progress.style.width = `${progressPercentage}%`

  updateButtons();
}

function updateButtons() {
  if (currentActive === 1) {
    prev.disabled = true
  } else if(currentActive === circles.length) {
    next.disabled = true;
  } else {
    prev.disabled = false;
    next.disabled = false;
  }
}