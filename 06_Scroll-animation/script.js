const boxes = document.querySelectorAll('.box')

window.addEventListener('scroll', showCheckBoxes)

function showCheckBoxes () {
  const fragmentedWindow = 5
  const triggerBottom = (window.innerHeight / fragmentedWindow) * 4

  boxes.forEach(box => {
    const boxTop = box.getBoundingClientRect().top
    box.children[1].textContent = ` Mark: ${triggerBottom} Top:${boxTop}`

    if(boxTop < triggerBottom) {
      box.classList.add('show')
    } else {
      box.classList.remove('show')
    }
  })
}