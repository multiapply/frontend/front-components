const draggableList = document.getElementById('draggable-list');
const check = document.getElementById('check');

const richestPeople = [
  'Elon Musk',
  'Jeff Bezos',
  'Bernard Arnault',
  'Bill Gates',
  'Larry Ellison',
  'Steve Ballmer',
  'Warren Buffett',
  'Larry Page',
  'Mark Zuckerberg',
  'Sergey Brin',
]

//Store DOM listitems
const listItems = [];

let dragStartIndex;

createDraggableList();

function unsortList(originalList){
  return [...originalList]
      .map(a =>({ value: a, order: Math.random()}))
      .sort((a,b) => a.order - b.order)
      .map(a => a.value)
}
//Insert list items into DOM
function createDraggableList() {
  const unsortedList = unsortList(richestPeople);
  unsortedList
    .forEach((person, index) => {
      const listItem = document.createElement('li');
      // listItem.classList.add('right');
      listItem.setAttribute('data-index', index);
      listItem.innerHTML = `
        <span class="number">${index + 1}</span>

        <div class="draggable" draggable="true">
          <p class="person-name">${person}</p>
          <i class="fas fa-grip-lines"></i>
        </div>
      `;
      //List
      listItems.push(listItem);

      //DOM
      draggableList.appendChild(listItem)
    })
    addEventListeners();
}

function dragStart() {
  // console.log('Event: ', 'dragStart');
  //closest: get closest-parent ancestor
  dragStartIndex = +this.closest('li').getAttribute('data-index');
}
function dragOver(e) {
  // console.log('Event: ', 'dragOver');
  //prevent default action/behaviour
  e.preventDefault();
}
function dragDrop() {
  // console.log('Event: ', 'dragDrop');
  const dragEndIndex = +this.getAttribute('data-index');
  swapItems(dragStartIndex, dragEndIndex);

  this.classList.remove('over');
}

function swapItems(fromIndex, toIndex) {
  // Get DOM Elements
  const itemOne = listItems[fromIndex].querySelector('.draggable');
  const itemTwo = listItems[toIndex].querySelector('.draggable');

  // append draggable element inside list item
  listItems[fromIndex].appendChild(itemTwo);
  listItems[toIndex].appendChild(itemOne);
}


function dragEnter() {
  // console.log('Event: ', 'dragEnter');
  this.classList.add('over');
}
function dragLeave() {
  // console.log('Event: ', 'dragLeave');
  this.classList.remove('over');
}


function addEventListeners() {
  const draggables = document.querySelectorAll('.draggable');
  const dragListItems = document.querySelectorAll('.draggable-list li');

  draggables.forEach(draggable => {
    draggable.addEventListener('dragstart', dragStart);
  });

  dragListItems.forEach(item => {
    item.addEventListener('dragover', dragOver);
    item.addEventListener('drop', dragDrop);
    item.addEventListener('dragenter', dragEnter);
    item.addEventListener('dragleave', dragLeave);
  });
}

function checkOrder() {
  listItems
    .forEach((listItem, index) => {
      const personName = listItem.querySelector('.draggable')
        .innerText.trim();
      
      if (personName !== richestPeople[index]) {
        listItem.classList.add('wrong');
      } else {
        listItem.classList.remove('wrong');
        listItem.classList.add('right');
      }
    });
}

check.addEventListener('click', checkOrder);