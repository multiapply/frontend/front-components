const template = document.createElement('div')
template.innerHTML = `
    <style>
        p {
            color: blue;
        }
        .texto {
            color: red;
        }
    </style>
    <p class="texto">Hola mundo 2</2>
    <p>Hola mundo 3, este es un texto de ejemplo</2>
`

// HTMLElement: API que ayuda a crear elementos custom
class myElement extends HTMLElement {
    constructor() {
        super(); //acceso a HTMLElement
        console.log('hola,hola')
        this.p = document.createElement('p');

    }

    connectedCallback() {
        this.p.textContent = 'Hola mundo!'
        // this.appendChild(this.p)
        // this.appendChild(template)
        this.append(this.p, template)
    }
}

// https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry
// window.customElements
customElements.define('my-element',myElement);
